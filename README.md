# Wordpress simple twig

Twig is pre-configured on wordpress `init`. Using the current theme location and it's `twig_templates` directory.

Note: `auto_reload` depends on `WP_DEBUG`

## Usage

Template location is from current theme root directory `twig_templates`

	echo twig_render(templateName, context);

## Advanced

Pre-configured Twig is currently registered as a global.. so do all you want...

	global $twig
	
	// do custom stuff
	
	$twig->something();
	

