<?php
/*
 * Plugin Name: Simple Twig
 *
 * Plugin URI: https://bitbucket.org/AubreyHewes/wp-simple-twig
 *
 * Description: Exposes Twig to Wordpress
 *
 * Author: Aubrey Hewes<aubrey@hewes.nl>
 * Author URI: https://bitbucket.org/AubreyHewes
 *
 * Version: 0.1
 * License: MIT
 *
 * Text Domain: wp-simple-twig
 *
 * @TODO remove global dependencies / restructure for isolated code
 *
 * @TODO proper plugin structure
 *
 * @TODO composer for lib dependencies
 *
 * @TODO tests maybe..?
 *
 */

/**
 * Twig init
 *
 * @throws Exception
 */
function wp_simple_twig_init() {
	global $twig; // we will globalise twig for the rest..

	if (!class_exists('Twig_Loader_Filesystem')) {
		throw new \Exception('You need to install Twig; Twig needs to be available on the classpath.');
	}

	// explicit loader on current theme dir
	$loader = new Twig_Loader_Filesystem(
		get_template_directory() . '/twig_templates'
	);

	$twig = new Twig_Environment($loader, array(

		// make unique (sic)
		'cache' => '/tmp/twig' . md5(get_template_directory() . implode('', $loader->getPaths())),

		// env needs to follow wp_core
		'auto_reload' => WP_DEBUG,

	));

}

/**
 * Twig render; not prefixed with wp_simple for usage reasons
 *
 * @param string $template
 * @param array $context
 *
 * @return string
 *
 * @throws Exception
 */
function twig_render($template, $context = array()) {
	global $twig; /* @var Twig_Environment $twig */

	if (!$twig instanceof Twig_Environment) {
		throw new \Exception('There is no Twig');
	}

	return $twig->loadTemplate($template)->render($context);
}

/**
 * Hook it
 */
add_action( 'init', 'wp_simple_twig_init' );

